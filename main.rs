fn main() {
    for i in 1..=100 {
        let mut out = String::from("");
        if i % 3 == 0 {
            out.push_str("Fizz");
        }
        if i % 5 == 0 {
            out.push_str("Buzz");
        }

        if out.len() == 0 {
            println!("{}", i);
        }else{
            println!("{}", out);
        }
    }
}